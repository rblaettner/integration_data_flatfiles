﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DocumentFormat.OpenXml;
using ClosedXML.Excel;

namespace MicrosoftOfficeExcel
{
    public enum DataStoreType
    {
        INDETERMINATE = 0,
        FLAT_FILE,
        SPREADSHEET,
        WEB_SERVICE,
        UNSPECIFIED
    }

    public class Service : MicrosoftOfficeExcel.IService
    {
        public string CreateNewSpreadSheet( FormatSource[] formats, File createFile, CreateFileOptions createFileOptions )
        {
            var target = new SpreadSheetTarget(formats, createFile, createFileOptions);

            target.open();
            target.close();

            return target.SpreadSheetSource.SpreadSheet.FilePathNameExt;
        }

        public bool ShiftRecordsAsPrependToSheet(SpreadSheet source, SpreadSheet target, KeyMap map )
        {
            var sourceRead = new SpreadSheetSource(source, map);
            var targetRead = new SpreadSheetSource(target, map);

            var sourceModify = new SpreadSheetTarget( sourceRead );
            var targetModify = new SpreadSheetTarget( targetRead );

            var records = new List<Record>();

            int sourceRecords = 0;
            int targetRecords = 0;

            sourceRead.open();
            sourceRecords = sourceRead.RecordCount;

            for (int i = 0; (i < sourceRecords); i++)
             {
                 records.Add(sourceRead.readRecord(i));
             }

            sourceRead.close();
            targetModify.open();
            targetRecords = records.Count();

            for (int i = (targetRecords - 1); (i >= 0); i--)
            {
                targetModify.insertRecord(records[i], 0, RelativePosition.INSERT_BEFORE);
            }

            targetModify.close();
            sourceModify.open();

            for (int i = 0; (i < sourceRecords); i++)
            {
                sourceModify.deleteRecord(i);
            }

            sourceModify.close();

            return true;

        }

        public int InsertRecordsIntoSpreadSheet(WebService source, SpreadSheetSource open, CreateFileOptions createFileOptions, int recordIndex)
        {
            int count = 0;

            var target = new SpreadSheetTarget(open);
            target.CreateFileOptions = createFileOptions;

            target.open();
            target.applyKeyMap();

            count += ( target.insertRecords( source.Records, recordIndex, RelativePosition.REPLACE_EXISTING ) );
       
            target.close();

            return count;
        }


        public int AppendRecordsToFlatFile(Record[] records, FlatFileSource open)
        {
            int count = 0;

            var target = new FlatFileTarget(open);

            target.Delimiters = new FlatFileExportDelimiters();

            target.open();
            target.applyKeyMap();

            count += (target.insertRecords(records, 0, RelativePosition.INSERT_LAST));

            target.close();

            return count;
        }


        public Record[] TestFlatFile()
        {
            var keyMap = new KeyMap();
            keyMap.Indices.Add(new MapByIndex(0));

            var flatFile = new FlatFile("C:\\Manifest.csv");
            var flatFileSource = new FlatFileSource(flatFile, keyMap);
            flatFileSource.FlatFileImportDelimiters.DelimiterForCols = (char)44;
            flatFileSource.FlatFileImportDelimiters.DelimiterForRows = (char)10;

            return ReadFlatFileRecords(flatFileSource, new int[] { 1, 2, 3, 4, 5 });
        }

        public Record[] ReadFlatFileRecords( FlatFileSource source, int[] recordIndices )
        {
            var records = new List<Record>();
            var record = new Record();

            source.open();
            // source.applyKeyMap();

           
            // try
            // {
                foreach (int recordIndex in recordIndices)
                {
                    record = source.readRecord(recordIndex);
                    records.Add(record);
                }
            // }
            

           // catch (Exception) { }
           
           

            return records.ToArray();
        }

        public Record[] ReadFlatFile( FlatFileSource source )
        {
            int index = 0;
            var records = new List<Record>();
            var record = new Record();

            source.open();

            record = source.readRecord( index );

            while( record.FieldData.Count() > 0 )
            {
                records.Add( record );
                record = source.readRecord( index++ );
            }

            source.close();
            return records.ToArray();
        }

        public Record[] ReadSpreadSheet(SpreadSheetSource source)
        {
            int index = 0;
            var records = new List<Record>();
            var record = new Record();

            source.open();

            record = source.readRecord(index);

            while (record.FieldData.Count() > 0)
            {
                records.Add(record);
                record = source.readRecord( index++ );
            }

            source.close();
            return records.ToArray();
        }

        public Record Test()
        {
            var records = new List<Record>();
            var webService = new WebService();

            var culture = new Culture();
            var transforms = new List<DataTransform>();

            var sourceIndices = new List<MapByIndex>();
            var sourceNames = new List<MapByName>();

            var sourceMap = new KeyMap();

            sourceIndices.Add(new MapByIndex(0));
            sourceIndices.Add(new MapByIndex(1));

            sourceMap.Indices = sourceIndices;

            var flatFileRead = new FlatFile("C:\\zip_tz.csv");
            var flatFileSource = new FlatFileSource(flatFileRead, sourceMap);

            flatFileSource.Delimiters = new FlatFileImportDelimiters();

            return FindRecordByFieldValueInFlatFile(flatFileSource, new MapByIndex(0), "603");
        }

        public Record FindRecordByFieldValueInFlatFile(FlatFileSource source, MapByIndex map, string value)
        {
            source.open();
            return source.findRecord(value, map);
        }
    }
}