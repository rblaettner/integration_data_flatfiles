﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ClosedXML.Excel;

/*
<![CDATA[C:\\Users\\ross.blaettner\\Documents\\manifest.csv]]>
<![CDATA[C:\\Users\\ross.blaettner\\Documents\\one.csv]]>
<![CDATA[C:\\Users\\ross blaettner\\Desktop\\one-one.csv]]>
*/

namespace MicrosoftOfficeExcel
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        bool ShiftRecordsAsPrependToSheet(SpreadSheet source, SpreadSheet target, KeyMap map);

        [OperationContract]
        int InsertRecordsIntoSpreadSheet(WebService source, SpreadSheetSource open, CreateFileOptions createFileOptions, int recordIndex);

        [OperationContract]
        string CreateNewSpreadSheet( FormatSource[] formats, File createFile, CreateFileOptions createFileOptions );

        [OperationContract]
        Record FindRecordByFieldValueInFlatFile(FlatFileSource source, MapByIndex map, string value);
    
        [OperationContract]
        Record[] ReadFlatFile(FlatFileSource source);

        [OperationContract]
        Record[] ReadSpreadSheet(SpreadSheetSource source);

        [OperationContract]
        Record[] ReadFlatFileRecords(FlatFileSource source, int[] recordIndices);

        [OperationContract]
        int AppendRecordsToFlatFile(Record[] records, FlatFileSource open);

        [OperationContract]
        Record Test();
    }

    [DataContract]
    public abstract class Store
    {
        protected FlatFile flatFile = null;
        protected WebService webService = null;
        protected SpreadSheet spreadSheet = null;
        //
        private Culture culture = new Culture();
        private KeyMap keyMap = new KeyMap();
        //
        private int startAtRowIndex = 0;
        private int startAtColIndex = 0;
        //

        public Store(KeyMap keyMap)
        {
            this.keyMap = keyMap;
        }

        public Record readRecord(int recordIndex)
        {
            var records = readRecords(new int[] { recordIndex });

            if (records.Count() > 0) return records[0];
            else return null;

        }

        public abstract List<Record> readRecords(int[] recordIndices);
        public abstract bool close();
        public abstract bool open();

        [DataMember]
        public Culture Culture
        {
            get { return this.culture; }
            set { this.culture = value; }
        }

        [DataMember]
        public KeyMap KeyMap
        {
            get { return this.keyMap; }
            set { this.keyMap = value; }
        }

        [DataMember]
        public int StartAtRowIndex
        {
            get { return this.startAtRowIndex; }
            set { this.startAtRowIndex = value; }
        }

        [DataMember]
        public int StartAtColIndex
        {
            get { return this.startAtColIndex; }
            set { this.startAtColIndex = value; }
        }

        public void applyKeyMap()
        {
            if ( this.keyMap.Names.Count() > this.KeyMap.Indices.Count() ) //// ???? (see comment block, below)
                applyKeyMap(this.keyMap);
        }

        private void applyKeyMap(KeyMap mapping)
        {
            var mapped = new List<MapByIndex>();
            /*
            foreach (var index in mapping.Indices)
            {
                var mapByIndex = new MapByIndex(index, name.Transforms);
                mapByIndex.crossIndex = (-1);
                mapped.Add(mapByIndex);
            }
            */

            if ( mapping == null ) return;
            //
            foreach (var name in mapping.Names)
            {
                var record = readRecord(name.CrossIndex);
                for (int index = 0; index < record.FieldData.Count; index++)
                {
                    if (record.FieldData[index].Value == name.Name)
                    {
                        var mapByIndex = new MapByIndex(index, name.Transforms);
                        mapByIndex.crossIndex = name.crossIndex;
                        mapped.Add(mapByIndex);
                    }
                }
            }
            //
            this.keyMap.Indices = mapped;
        }

        protected Position getPosition(int recordIndex, int totalRecords, MapByIndex mapIndex)
        {
            var position = new Position();

            if (this.culture.getRecordIndex(recordIndex, mapIndex.CrossIndex, totalRecords) < 0) return position;

            switch (this.culture.RecordOrientation)
            {
                case RecordOrientation.ROW:
                    position.Y = (this.startAtRowIndex + this.culture.getRecordIndex(recordIndex, mapIndex.CrossIndex, totalRecords));
                    position.X = (this.startAtColIndex + mapIndex.Index);
                    break;

                case RecordOrientation.COLUMN:
                    position.X = (this.startAtColIndex + this.culture.getRecordIndex(recordIndex, mapIndex.CrossIndex, totalRecords));
                    position.Y = (this.startAtRowIndex + mapIndex.Index);
                    break;
            }
            return position;
        }
    }

    [DataContract]
    public class Culture
    {

        private int incrementBy = 1;

        [DataMember]
        public RecordOrientation RecordOrientation { get; set; }

        /* RESERVED */
        // public DataOrderHorizontal DataOrderHorizontal { get; set; }
        /* RESERVED */

        [DataMember]
        public DataOrderVertical DataOrderVertical
        {
            get { return this.dataOrderVertical; }
            set { this.dataOrderVertical = value; }
        }

        private DataOrderVertical dataOrderVertical = DataOrderVertical.TOP_TO_BOTTOM;

        [DataMember]
        public int IncrementRecordCountBy
        {
            get
            {
                return (this.DataOrderVertical == DataOrderVertical.TOP_TO_BOTTOM) ?
                    this.incrementBy : (this.incrementBy * (-1));
            }
            set { this.incrementBy = Math.Abs(value); }
        }

        public int getRecordIndex(int currentRecordIndex, int crossIndex, int totalRecords)
        {
            int updatedRecordIndex = currentRecordIndex;


            if (crossIndex == (0))  // ( should be -1 ) // see crossIndex at class instantiation
            {
                switch (this.DataOrderVertical)
                {
                    case DataOrderVertical.TOP_TO_BOTTOM:
                        if (updatedRecordIndex > (totalRecords - 1)) return -1;
                        break;

                    case DataOrderVertical.BOTTOM_TO_TOP:
                        if (updatedRecordIndex < 0) return -1;
                        break;
                }
            }
            else if (updatedRecordIndex >= crossIndex)
            {
                switch (this.DataOrderVertical)
                {
                    case DataOrderVertical.TOP_TO_BOTTOM:
                        return (updatedRecordIndex + 1);

                    case DataOrderVertical.BOTTOM_TO_TOP:
                        return (updatedRecordIndex);
                }
            }

            else if (updatedRecordIndex <= crossIndex)
            {
                switch (this.DataOrderVertical)
                {
                    case DataOrderVertical.TOP_TO_BOTTOM:
                        return (updatedRecordIndex);

                    case DataOrderVertical.BOTTOM_TO_TOP:
                        return (updatedRecordIndex - 1);
                }
            }

            switch (this.DataOrderVertical)
            {
                case DataOrderVertical.TOP_TO_BOTTOM:
                    if (updatedRecordIndex > (totalRecords - 1)) return -1;
                    break;

                case DataOrderVertical.BOTTOM_TO_TOP:
                    if (updatedRecordIndex < 0) return -1;
                    break;
            }

            return updatedRecordIndex;
        }
    }

    [DataContract]
    public enum DataOrderVertical { [EnumMember] TOP_TO_BOTTOM = 0,  [EnumMember] BOTTOM_TO_TOP = 1 };

    [DataContract]
    public enum DataOrderHorizontal { [EnumMember] LEFT_TO_RIGHT = 0, [EnumMember] RIGHT_TO_LEFT = 1 };

    [DataContract]
    public enum RecordOrientation
    {
        [EnumMember]
        ROW,

        [EnumMember]
        COLUMN
    }

    [DataContract]
    public abstract class DataStoreSource : Store
    {
        [DataMember]
        public DataSourceImport DataSourceImport { get; set; }

        public DataStoreSource(KeyMap keyMap) : base(keyMap) { }

    }

    [DataContract]
    public abstract class DataStoreTarget : Store
    {
        protected DataStoreSource dataStoreSource;

        protected List<Record> Transactions { get; set; }

        public DataStoreTarget(DataStoreSource source)
            : base(source.KeyMap)
        {
            dataStoreSource = source;
            base.Culture = source.Culture;
            base.StartAtRowIndex = source.StartAtRowIndex;
            base.StartAtColIndex = source.StartAtColIndex;
        }        


        public DataStoreTarget( Culture culture, int startAtRowIndex, int startAtColIndex, KeyMap keyMap ) : base(keyMap)
        {
            base.Culture = culture;
            base.StartAtRowIndex = startAtRowIndex;
            base.StartAtColIndex = startAtColIndex;
        }

        public void addTransaction(Record original)
        {
            this.Transactions.Add(original);
        }

        public void rollBack() { }

        public override List<Record> readRecords(int[] recordIndices)
        {
            if (this.dataStoreSource != null)
                return this.dataStoreSource.readRecords(recordIndices);
            else return new List<Record>();
        }

        public abstract Record deleteRecord(int recordIndex);
        public abstract bool insertRecord(Record record, int recordIndex, RelativePosition position);
        public abstract Record updateRecord(int recordIndex, Record record);

        public List<Record> updateRecords(int[] recordIndices, Record record)
        {
            var originals = new List<Record>();
            // recordIndices.Reverse();

            foreach (var recordIndex in recordIndices)
                originals.Add(updateRecord(recordIndex, record));

            return originals;
        }

        public int insertRecords(Record[] records, int recordIndex, RelativePosition position)
        {
            int count = 0;
            // records.Reverse();

            foreach (var record in records)
            {
                if (insertRecord(record, recordIndex, position))
                    count++;
            }

            return count;
        }

    }

    [DataContract]
    public class Map
    {

        private List<DataTransform> transforms = new List<DataTransform>();

        [DataMember]
        public List<DataTransform> Transforms
        {
            get { return this.transforms; }
            set { this.transforms = value; }
        }

        public Map()
        {
            this.transforms = new List<DataTransform>();
            this.transforms.Add(DataTransform.NONE);
        }

        public Map(List<DataTransform> transforms)
        {
            this.transforms = transforms;
        }

        public string applyTransforms(string fieldValue)
        {
            string transformation = fieldValue;

            foreach (var transform in this.transforms)
            {
                switch (transform)
                {
                    case DataTransform.NONE:
                        break;

                    case DataTransform.SCIENTIFIC_TO_INTEGER:

                        transformation = ((int)(double.Parse(transformation, (System.Globalization.NumberStyles.AllowExponent | System.Globalization.NumberStyles.AllowDecimalPoint)))).ToString();
                        break;

                    default: return transformation;
                }
            }
            return transformation;
        }
    }

    [DataContract]
    public class MapByIndex : Map
    {
        public MapByIndex(int index, List<DataTransform> transforms)
            : base(transforms)
        {
            this.Index = index;
        }

        public MapByIndex(int index)
            : base()
        {
            this.Index = index;
        }

        [DataMember]
        public int Index { get; set; }
        public int crossIndex = (-1);

        public int CrossIndex
        {
            get { return this.crossIndex; }
            set { this.crossIndex = value; }
        }
    }

    [DataContract]
    public class MapByName : Map
    {

        [DataMember]
        public string Name { get; set; }
        public int crossIndex = 0;

        [DataMember]
        public int CrossIndex
        {
            get { return this.crossIndex; }
            set { this.crossIndex = value; }
        }

        public MapByName(string name)
            : base()
        {
            this.Name = name;
        }

        public MapByName(string name, int crossIndex)
            : this(name)
        {
            this.crossIndex = crossIndex;
        }

        public MapByName(string name, int crossIndex, List<DataTransform> transform)
            : base(transform)
        {
            this.crossIndex = crossIndex;
            this.Name = name;
        }
    }

    public class KeyMap
    {
        public List<MapByIndex> Indices
        {
            get { return this.indices; }
            set { this.indices = value; }
        }

        public List<MapByName> Names
        {
            get { return this.names; }
            set { this.names = value; }
        }

        private List<MapByIndex> indices = new List<MapByIndex>();
        private List<MapByName> names = new List<MapByName>();

        public void setIndices(int count)
        {
            var indices = new List<MapByIndex>();
            //
            for (int i = 0; i < count; i++)
                indices.Add(new MapByIndex(i));
            //
            this.indices = indices;
        }
    }

    [DataContract]
    public enum DataTransform
    {
        [EnumMember]
        NONE = 0,

        [EnumMember]
        SCIENTIFIC_TO_INTEGER
    }

    [DataContract]
    public class WebServiceTarget : DataStoreTarget
    {

        public WebServiceTarget(WebServiceSource source)
            : base(source)
        {
            base.webService = source.WebService;
            open();
        }

        [DataMember]
        public WebServiceSource WebServiceSource
        {
            get { return (WebServiceSource)base.dataStoreSource; }
            set { base.dataStoreSource = value; }
        }
        public override bool open()
        {
            return true;
        }

        public override bool close()
        {
            return true;
        }

        public override Record deleteRecord(int recordIndex)
        {
            return new Record();
        }
        public override bool insertRecord(Record record, int recordIndex, RelativePosition position)
        {
            return true;
        }
        public override Record updateRecord(int recordIndex, Record record)
        {
            return new Record();
        }
    }

    [DataContract]
    public class FlatFileTarget : DataStoreTarget
    {
        [DataMember]
        public FlatFileExportDelimiters Delimiters { get; set; }

        [DataMember]
        public CreateFileOptions CreateFileOptions { get; set; }

        public FlatFileTarget(FlatFileSource source)
            : base(source)
        {
            base.flatFile = new FlatFile( source.FlatFile.FilePathNameExt );
            // open();
        }

        public FlatFileSource FlatFileSource
        {
            get { return (FlatFileSource)base.dataStoreSource; }
            set { base.dataStoreSource = value; }
        }

        public override bool open()
        {
            var flatFileSource = ((FlatFileSource)base.dataStoreSource);
            return base.flatFile.openWrite();
        }

        public override bool close()
        {
            return base.flatFile.close();
        }

        public override List<Record> readRecords(int[] recordIndices)
        {
            return base.readRecords(recordIndices);
        }

        public override Record deleteRecord(int recordIndex)
        {
            return new Record();
        }


        public override bool insertRecord(Record record, int recordIndex, RelativePosition position)
        {
            close();

            int totalRecords = this.FlatFileSource.RecordCount;
            var point = base.getPosition(recordIndex, totalRecords, base.KeyMap.Indices[0]);
            var inputLines = System.IO.File.ReadLines(base.flatFile.FilePathNameExt).ToList<string>();

            // if (!point.isSpecified()) return false;

            switch (base.Culture.RecordOrientation)
            {
                case RecordOrientation.ROW:

                    int oldLineIndex = Math.Max( 0, Math.Max( ( totalRecords - 1 ), point.Y ) );

                    switch (position)
                    {

                        case RelativePosition.INSERT_BEFORE:

                            oldLineIndex = ( point.Y );
                            break;

                        case RelativePosition.INSERT_AFTER:

                            oldLineIndex = ( point.Y + 1 );
                            break;

                        case RelativePosition.REPLACE_EXISTING:

                            updateRecord( recordIndex, record );
                            break;

                    }

                    string oldLineText = ( oldLineIndex < inputLines.Count() ) ? inputLines[oldLineIndex] : "";
                    string newLineText = "";

                    // oldLineText = this.FlatFileSource.Delimiters.applyTo(oldLineText);

                    // int delimiters = ( oldLineText.Split(Delimiters.DelimiterForCols).Count() - 1 );
                    int delimiters = ( base.KeyMap.Indices.Count() - 1);

                    for ( int i = 0; ( i < delimiters ); i++) { newLineText += this.Delimiters.DelimiterForCols; }

                    // newLineText = this.Delimiters.applyTo(newLineText);

                    inputLines.Reverse();

                    inputLines.Insert( ( oldLineIndex ), newLineText );

                    // inputLines.Reverse();

                    System.IO.File.WriteAllLines( base.flatFile.FilePathNameExt, inputLines );
                    updateRecord( ( oldLineIndex ), record );

                    break;

                case RecordOrientation.COLUMN:

                    switch (position)
                    {
                        case RelativePosition.INSERT_BEFORE:
                            break;

                        case RelativePosition.INSERT_AFTER:
                            break;

                        case RelativePosition.REPLACE_EXISTING:

                            updateRecord( point.X, record );
                            
                            break;
                    }
                    break;
            }
            open();
            return true;
        }

        public override Record updateRecord(int recordIndex, Record record)
        {

            this.FlatFileSource.open();

            var original = readRecord(recordIndex);

            this.FlatFileSource.close();

            int totalRecords = this.FlatFileSource.RecordCount;
            var point = base.getPosition(recordIndex, totalRecords, base.KeyMap.Indices[0]);
            var inputLines = System.IO.File.ReadLines(base.flatFile.FilePathNameExt).ToList<string>();
            string oldLineText = inputLines[recordIndex];

            // open();

            oldLineText = this.FlatFileSource.Delimiters.applyTo(oldLineText);
            string[] lineFields = oldLineText.Split(Delimiters.DelimiterForCols);

            foreach( var mapping in base.KeyMap.Indices )
            {
                lineFields[mapping.Index] = (string.IsNullOrWhiteSpace(record.FieldData[mapping.Index].Value)) ? "" : record.FieldData[mapping.Index].Value;
            }

            string newLineText = String.Join(Delimiters.DelimiterForCols.ToString(), lineFields);

            newLineText = this.FlatFileSource.Delimiters.applyTo(newLineText);
            inputLines[recordIndex] = newLineText;

            // open();

           
            System.IO.File.WriteAllLines(base.flatFile.FilePathNameExt, inputLines);

            // close();

            return original;
        }
    }

    [DataContract]
    public class SpreadSheetTarget : DataStoreTarget
    {

        [DataMember]
        public CreateFileOptions CreateFileOptions { get; set; }

        private FormatSource[] FormatSources { get; set; }

        public SpreadSheetTarget(SpreadSheetSource source)
            : base(source)
        {
            base.spreadSheet = source.SpreadSheet;
            open();
        }

        public override Record deleteRecord(int recordIndex)
        {
            Record emptyRecord = new Record();
            Record originalData = ((SpreadSheetSource)base.dataStoreSource).readRecord(recordIndex);
            //
            int totalRecords = ((SpreadSheetSource)base.dataStoreSource).RecordCount;
            //
            for (int fieldIndex = 0; (fieldIndex < base.KeyMap.Indices.Count()); fieldIndex++)
            {
                emptyRecord.addFieldData(fieldIndex, "");
            }
            //
            updateRecord(recordIndex, emptyRecord);
            return originalData;
        }

        public void restoreFromFormatSource()
        {
            int totalRecords = ((SpreadSheetSource)base.dataStoreSource).RecordCount;
 
            if ( this.FormatSource != null)
            {
                var formatSpreadSheet = this.FormatSource.SpreadSheet;

                if (!formatSpreadSheet.openRead()) return;

                for (int recordIndex = 0; (recordIndex < totalRecords); recordIndex++)
                {
                    foreach (var mapIndex in base.KeyMap.Indices)
                    {
                        var stylePoint = base.getPosition(0, totalRecords, mapIndex);
                        var dataPoint = base.getPosition(recordIndex, totalRecords, mapIndex);

                        if (dataPoint.isSpecified())
                        {
                            var formatCell = formatSpreadSheet.WorkSheet.Cell((stylePoint.Y + 1), (stylePoint.X + 1));
                            var sourceCell = formatSpreadSheet.WorkSheet.Cell((dataPoint.Y + 1), (dataPoint.X + 1));
                            var targetCell = base.spreadSheet.WorkSheet.Cell((dataPoint.Y + 1), (dataPoint.X + 1));
 
                            targetCell.Value = sourceCell.Value;
                            targetCell.Style = formatCell.Style;
                        }
                    }
                }
                //
                formatSpreadSheet.close();
            }
        }

        public SpreadSheetTarget( FormatSource formatSource ) : base( new SpreadSheetSource( formatSource.SpreadSheet, formatSource.KeyMap ) )
        {
            this.FormatSource = formatSource;
            //
            var targetFile = formatSource.TargetFile;
            var sheetName = formatSource.SpreadSheet.WorkSheetName;
            //
            base.spreadSheet = new SpreadSheet(targetFile.FilePathNameExt, sheetName);
            base.dataStoreSource = new SpreadSheetSource(base.spreadSheet, formatSource.KeyMap); //replace
            open();
        }

        public SpreadSheetTarget(FormatSource[] formats, File createFile, CreateFileOptions createFileOptions)
            : this(getFormatSource(formats, createFile, createFileOptions))
        {
            this.FormatSources = formats;
        }

        public static FormatSource getFormatSource( FormatSource[] formats, File createFile, CreateFileOptions createFileOptions )
        {
            foreach (var format in formats)
            {
                format.TargetFile = (format.SpreadSheet.File.copyFileTo(createFile, createFileOptions));
                if (format.TargetFile != null) { return format; }
            }
            //
            return null;
        }

        public FormatSource FormatSource { get; set; }

        public SpreadSheetSource SpreadSheetSource
        {
            get { return (SpreadSheetSource)base.dataStoreSource; }
            set { base.dataStoreSource = value; }
        }

        public bool[] protections = new bool[] { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

        public override bool open()
        {
            var spreadSheetSource = ((SpreadSheetSource)base.dataStoreSource);
            spreadSheetSource.open();
            bool success = base.spreadSheet.openWrite();

            // var protection = base.spreadSheet.WorkSheet.Protection;

            /*
            this.protections = new bool[]
                    {
                        base.spreadSheet.WorkSheet.Protection.SelectLockedCells,
                        base.spreadSheet.WorkSheet.Protection.SelectUnlockedCells,
                        base.spreadSheet.WorkSheet.Protection.FormatCells,
                        base.spreadSheet.WorkSheet.Protection.FormatColumns,
                        base.spreadSheet.WorkSheet.Protection.FormatRows,
                        base.spreadSheet.WorkSheet.Protection.InsertColumns,
                        base.spreadSheet.WorkSheet.Protection.InsertRows,
                        base.spreadSheet.WorkSheet.Protection.InsertHyperlinks,
                        base.spreadSheet.WorkSheet.Protection.DeleteColumns,
                        base.spreadSheet.WorkSheet.Protection.DeleteRows,
                        base.spreadSheet.WorkSheet.Protection.Sort,
                        base.spreadSheet.WorkSheet.Protection.AutoFilter,
                        base.spreadSheet.WorkSheet.Protection.PivotTables,
                        base.spreadSheet.WorkSheet.Protection.Objects,
                        base.spreadSheet.WorkSheet.Protection.Scenarios
                    };
             */ 

            if (success && !(string.IsNullOrEmpty(spreadSheetSource.Password)) && base.spreadSheet.WorkSheet.Protection.Protected )
            {
                try
                {
                    base.spreadSheet.WorkSheet.Protection.Unprotect(spreadSheetSource.Password);
                }
                //
                catch (System.ArgumentException) { spreadSheet.close(); return false; }
                //
                return (!( base.spreadSheet.WorkSheet.Protection.Protected ) && success );
            }
            else { return success;  }
        }

        public override bool close()
        {
            var spreadSheetSource = ((SpreadSheetSource)base.dataStoreSource);
            //
            if (!(string.IsNullOrEmpty(spreadSheetSource.Password)) && !(base.spreadSheet.WorkSheet.Protection.Protected) )
            {

                base.spreadSheet.WorkSheet.Protection.Protect(spreadSheetSource.Password);

                base.spreadSheet.WorkSheet.Protection.SetSelectLockedCells(this.protections[0]);
                base.spreadSheet.WorkSheet.Protection.SetSelectUnlockedCells(this.protections[1]);
                base.spreadSheet.WorkSheet.Protection.SetFormatCells(this.protections[2]);
                base.spreadSheet.WorkSheet.Protection.SetFormatColumns(this.protections[3]);
                base.spreadSheet.WorkSheet.Protection.SetFormatRows(this.protections[4]);
                base.spreadSheet.WorkSheet.Protection.SetInsertColumns(this.protections[5]);
                base.spreadSheet.WorkSheet.Protection.SetInsertRows(this.protections[6]);
                base.spreadSheet.WorkSheet.Protection.SetInsertHyperlinks(this.protections[7]);
                base.spreadSheet.WorkSheet.Protection.SetDeleteColumns(this.protections[8]);
                base.spreadSheet.WorkSheet.Protection.SetDeleteRows(this.protections[9]);
                base.spreadSheet.WorkSheet.Protection.SetSort(this.protections[10]);
                base.spreadSheet.WorkSheet.Protection.SetAutoFilter(this.protections[11]);
                base.spreadSheet.WorkSheet.Protection.SetPivotTables(this.protections[12]);
                base.spreadSheet.WorkSheet.Protection.SetObjects(this.protections[13]);
                base.spreadSheet.WorkSheet.Protection.SetScenarios(this.protections[14]);

                return (base.spreadSheet.WorkSheet.Protection.Protected && base.spreadSheet.close());
            }
            //
            else { return base.spreadSheet.close(); }
        }

        public override bool insertRecord(Record record, int recordIndex, RelativePosition position)
        {
            var point = new Position();
            int totalRecords = ((SpreadSheetSource)base.dataStoreSource).RecordCount;

            point = base.getPosition(recordIndex, totalRecords, base.KeyMap.Indices[0]);

            switch (base.Culture.RecordOrientation)
            {
                case RecordOrientation.ROW:

                    switch (position)
                    {
                        case RelativePosition.INSERT_BEFORE:

                            base.spreadSheet.WorkSheet.Row(point.Y + 1).InsertRowsAbove(1);
                            updateRecord(recordIndex, record);
                            break;

                        case RelativePosition.INSERT_AFTER:
                            base.spreadSheet.WorkSheet.Row(point.Y + 1).InsertRowsBelow(1);
                            updateRecord(recordIndex + 1, record);
                            break;

                        case RelativePosition.REPLACE_EXISTING:
                            updateRecord(recordIndex, record);
                            break;
                    }
                    break;

                case RecordOrientation.COLUMN:

                    switch (position)
                    {
                        case RelativePosition.INSERT_BEFORE:

                            base.spreadSheet.WorkSheet.Column(point.X + 1).InsertColumnsBefore(1);
                            updateRecord(recordIndex, record);
                            break;

                        case RelativePosition.INSERT_AFTER:
                            base.spreadSheet.WorkSheet.Column(point.X + 1).InsertColumnsBefore(1);
                            updateRecord(recordIndex + 1, record);
                            break;

                        case RelativePosition.REPLACE_EXISTING:
                            updateRecord(recordIndex, record);
                            break;
                    }
                    break;
            }
            return true;
        }
        public override Record updateRecord(int recordIndex, Record record)
        {
            Record original = readRecord(recordIndex);
            int totalRecords = ((SpreadSheetSource)base.dataStoreSource).RecordCount;


            for(int i = 0; ( i < record.FieldData.Count() ); i++ )
            {
                var entry = (string.IsNullOrWhiteSpace(record.FieldData[i].Value)) ? "" : record.FieldData[i].Value;
                var point = base.getPosition(recordIndex, totalRecords, base.KeyMap.Indices[i]);
                base.spreadSheet.WorkSheet.Cell((point.Y + 1), (point.X + 1)).SetValue<string>(entry);
            }

            return original;
        }



    }



    [DataContract]
    public enum RelativePosition
    {
        [EnumMember]
        REPLACE_EXISTING = 0,

        [EnumMember]
        INSERT_BEFORE = (-1),

        [EnumMember]
        INSERT_AFTER = (+1),

        [EnumMember]
        INSERT_LAST = (+2)
    }

    [DataContract]
    public class WebServiceSource : DataStoreSource
    {

        public override bool open()
        {
            return (base.webService.Records.Count() > 0);
        }

        public override bool close()
        {
            return true;
        }

        [DataMember]
        public WebService WebService
        {
            get { return base.webService; }
            set { base.webService = value; }
        }

        public WebServiceSource(WebService webService, KeyMap keyMap)
            : base(keyMap)
        {
            base.webService = webService;
        }

        public override List<Record> readRecords(int[] recordIndices)
        {
            var outputRecords = new List<Record>();

            return outputRecords;
        }
    }

    [DataContract]
    public class FlatFileSource : DataStoreSource
    {
        public FlatFileImportDelimiters Delimiters = new FlatFileImportDelimiters();

        [DataMember]
        public FlatFileImportDelimiters FlatFileImportDelimiters
        {
            get { return this.Delimiters; }
            set { this.Delimiters = value; }
        }
        [DataMember]
        public bool DeleteFileOnClose { get; set; }

        public FlatFileSource(FlatFile flatFile, KeyMap keyMap)
            : base(keyMap)
        {
            base.flatFile = flatFile;
        }

        [DataMember]
        public FlatFile FlatFile
        {
            get { return base.flatFile; }
            set { base.flatFile = value; }
        }

        public override bool open()
        {
            bool success = base.flatFile.openRead();
            if (success) applyKeyMap();
                return success;
  
        }

        public override bool close()
        {
            return base.flatFile.close();
        }

        public Record findRecord( string value, MapByIndex map )
        {

            int iMin = 0;
            int iMax = (this.RecordCount - 1);

            return findRecord( value, map, iMin, iMax );
        }

        public Record findRecord( string value, MapByIndex map, int iMin, int iMax )
        {
            int startAtRowIndex = base.StartAtRowIndex;
            int startAtColIndex = base.StartAtColIndex;

            var outputRecord = new Record();

            if ( iMax < iMin )
                return outputRecord;
            else
            {
                int iMid = (iMin + iMax) / 2;

                if (int.Parse(this.readRecord(iMid).FieldData[map.Index].Value) > int.Parse(value))
                    return findRecord(value, map, iMin, (iMid - 1));
                else if (int.Parse(this.readRecord(iMid).FieldData[map.Index].Value) < int.Parse(value))
                    return findRecord(value, map, (iMid + 1), iMax);
                else
                    return readRecord(iMid);
            }
        }

        public int RecordCount
        {
            get
            {

                var inputLines = System.IO.File.ReadLines(base.flatFile.FilePathNameExt).ToList();

                switch (base.Culture.RecordOrientation)
                {
                    case RecordOrientation.ROW:

                        foreach (var map in base.KeyMap.Indices)
                        {
                            var position = base.getPosition((inputLines.Count() - 1), inputLines.Count(), map);
                            if (!position.isSpecified()) return 0;
                            else return ( position.Y + 1 );
                        }
                        break;

                    case RecordOrientation.COLUMN:

                        foreach (var map in base.KeyMap.Indices)
                        {
                            var position = base.getPosition(inputLines.Count(), inputLines[0].Split(this.Delimiters.DelimiterForCols).Length, map);
                            if (!position.isSpecified()) return 0;
                            else return ( position.X + 1 );
                        }
                        break;
                }

                return 0;
            }
        }

        public override List<Record> readRecords(int[] recordIndices)
        {
            var outputRecord = new Record();
            var outputRecords = new List<Record>();
            int recordCount = this.RecordCount;

            close();

            var inputLines = System.IO.File.ReadLines(base.flatFile.FilePathNameExt).ToList<string>();

            open();

            

            if ( recordCount == 0 ) return outputRecords;

            switch (base.Culture.RecordOrientation)
            {
                case RecordOrientation.ROW:

                    if ((base.KeyMap.Indices.Count() == 0) && (recordIndices.Length == 1))
                    {
                        foreach (var field in inputLines[recordIndices[0]].Split(this.Delimiters.DelimiterForCols))
                            outputRecord.addFieldData(outputRecord.FieldData.Count, field);
                        outputRecords.Add(outputRecord);
                    }
                    else
                    {
                        foreach (var recordIndex in recordIndices)
                        {
                            //
                            foreach (var map in base.KeyMap.Indices)
                            {
                                var position = base.getPosition(recordIndex, recordCount, map);
                                if (!position.isSpecified()) break;
                                else
                                {
                                    string inputLine = inputLines[position.Y]; /* this.Delimiters.applyTo(inputLines[position.Y]); */
                                    outputRecord.addFieldData(map.Index, map.applyTransforms(inputLine.Split(this.Delimiters.DelimiterForCols)[position.X]));
                                }
                            }
                            //
                            outputRecords.Add(outputRecord);
                            outputRecord = new Record();
                        }
                    }
                    //
                    break;
                //
                case RecordOrientation.COLUMN:

                    if ((base.KeyMap.Indices.Count() == 0) && (recordIndices.Length == 1))
                    {
                        foreach (var line in inputLines)
                            outputRecord.addFieldData(outputRecord.FieldData.Count, line.Split(this.Delimiters.DelimiterForCols)[recordIndices[0]]);
                        outputRecords.Add(outputRecord);
                    }
                    else
                    {
                        foreach (var recordIndex in recordIndices)
                        {
                            outputRecord = new Record();
                            foreach (var map in base.KeyMap.Indices)
                            {
                                var position = base.getPosition(recordIndex, inputLines[map.Index].Split(this.Delimiters.DelimiterForCols).Length, map);
                                if (!position.isSpecified()) break;
                                else outputRecord.addFieldData(map.Index, inputLines[position.Y].Split(this.Delimiters.DelimiterForCols)[position.X]);
                            }
                            outputRecords.Add(outputRecord);
                        }
                    }
                    //
                    break;
            }
            //
            return outputRecords;
        }
    }



    [DataContract]
    public class SpreadSheetSource : DataStoreSource
    {
        [DataMember]
        public bool DeleteFileOnClose { get; set; }

        public override bool open()
        {
            return base.spreadSheet.openRead();
        }

        public XLWorksheetVisibility visibility;

        public override bool close()
        {
            return base.spreadSheet.close();
        }

        public SpreadSheetSource(SpreadSheet spreadSheet, KeyMap keyMap)
            : base(keyMap)
        {
            base.spreadSheet = spreadSheet;
        }

        public SpreadSheetSource(SpreadSheet spreadSheet, KeyMap keyMap, string password)
            : this(spreadSheet, keyMap)
        {
            this.Password = password;
            base.spreadSheet = spreadSheet;
        }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public SpreadSheet SpreadSheet
        {
            get { return base.spreadSheet; }
            set { base.spreadSheet = value; }
        }

        public int RecordCount
        {
            get
            {
                switch (base.Culture.RecordOrientation)
                {
                    case RecordOrientation.ROW:
                        return base.spreadSheet.WorkSheet.LastRowUsed().RowNumber();

                    case RecordOrientation.COLUMN:
                        return base.spreadSheet.WorkSheet.LastColumnUsed().ColumnNumber();

                    default: return 0;
                }
            }
       
        }

        public override List<Record> readRecords(int[] recordIndices)
        {
            var outputRecord = new Record();
            var outputRecords = new List<Record>();
            int totalRecords = this.RecordCount;

            if ((base.KeyMap.Indices.Count() == 0) && (recordIndices.Length == 1))
            {
                switch (base.Culture.RecordOrientation)
                {
                    case RecordOrientation.ROW:
                        for (int columnIndex = 0; (columnIndex < base.spreadSheet.WorkSheet.LastColumnUsed().ColumnNumber()); columnIndex++)
                            outputRecord.addFieldData(columnIndex, base.spreadSheet.WorkSheet.Cell((recordIndices[0] + 1), (columnIndex + 1)).GetValue<string>());
                        outputRecords.Add(outputRecord);
                        break;

                    case RecordOrientation.COLUMN:
                        for (int rowIndex = 0; (rowIndex < base.spreadSheet.WorkSheet.LastRowUsed().RowNumber()); rowIndex++)
                            outputRecord.addFieldData(rowIndex, base.spreadSheet.WorkSheet.Cell((rowIndex + 1), (recordIndices[0] + 1)).GetValue<string>());
                        outputRecords.Add(outputRecord);
                        break;
                }
            }
            else
            {
                foreach (var recordIndex in recordIndices)
                {
                    foreach (var map in base.KeyMap.Indices)
                    {
                        var position = base.getPosition(recordIndex, totalRecords, map);

                        outputRecord.addFieldData(map.Index, map.applyTransforms(base.spreadSheet.WorkSheet.Cell((position.Y + 1), (position.X + 1)).GetValue<string>()));
                    }
                    outputRecords.Add(outputRecord);
                    outputRecord = new Record();
                }
            }
            return outputRecords;
        }
    }


    [DataContract]
    public class WebService : DataStore
    {
        [DataMember]
        public Record[] Records { get; set; }
    }

    [DataContract]
    public class Record
    {
        public Record()
        {
            fieldData = new List<KeyValuePair<int, string>>();
        }

        [DataMember]
        public List<KeyValuePair<int, string>> FieldData
        {
            get { return this.fieldData; }
            set { this.fieldData = value; }
        }

        private List<KeyValuePair<int, string>> fieldData;

        public void addFieldData(int fieldIndex, string value)
        {
            this.fieldData.Add(new KeyValuePair<int, string>(fieldIndex, value));
        }
    }

    [DataContract]
    public class Field
    {
        [DataMember]
        public string Value { get; set; }

        public Field(string value)
        {
            this.Value = value;
        }
    }

    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Position() : this(-1, -1) { }

        public Position(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public bool isSpecified()
        {
            if (this.X == (-1)) return false;
            if (this.Y == (-1)) return false;
            //
            return true;
        }
    }

    [DataContract]
    public abstract class DataStore { }

    [DataContract]
    public enum CountEmptyRecordsMethod
    {
        [EnumMember]
        ALL_IN_TOTAL,

        [EnumMember]
        ONLY_CONSEQUTIVE
    }

    [DataContract]
    public class DataSourceImport
    {
        private bool skipEmptyRecords = true;
        private int stopAtEmptyRecordCount = 0; // 0 : never stop, until EOF // -1 : do not import rows

        [DataMember]
        public int ImportRecordsMaxCount { get; set; }

        [DataMember]
        public bool SkipEmptyRecords
        {
            get { return this.skipEmptyRecords; }
            set { this.skipEmptyRecords = value; }
        }

        [DataMember]
        public int StopAtEmptyRecordCount
        {
            get { return this.stopAtEmptyRecordCount; }
            set { this.stopAtEmptyRecordCount = value; }
        }

        [DataMember]
        public CountEmptyRecordsMethod CountEmptyRecordsMethod { get; set; }

        public bool isEndOfRecords(int totalRecords, int emptyRecordsTotal, int emptyRecordsConsequtive)
        {
            if (totalRecords > this.ImportRecordsMaxCount) return true;
            else switch (this.CountEmptyRecordsMethod)
                {
                    case CountEmptyRecordsMethod.ALL_IN_TOTAL:
                        return (emptyRecordsTotal > this.stopAtEmptyRecordCount);

                    case CountEmptyRecordsMethod.ONLY_CONSEQUTIVE:
                        return (emptyRecordsConsequtive > this.stopAtEmptyRecordCount);

                    default: return false;
                }
        }
    }

    [DataContract]
    public class SpreadSheet : File
    {
        private XLWorkbook workBook;
        private IXLWorksheet workSheet;

        [DataMember]
        public string WorkSheetName { get; set; }

        public File File
        {
            get { return (File)this; }
        }

        public IXLWorksheet WorkSheet { get { return this.workSheet; } }
        public XLWorkbook WorkBook { get { return this.workBook; } }

        public SpreadSheet(string filePathNameExt, string workSheet) : this(filePathNameExt, workSheet, false) { }

        public SpreadSheet(string filePathNameExt, string workSheet, bool deleteOnClose)
            : base(filePathNameExt)
        {
            this.WorkSheetName = workSheet;
            base.DeleteAfterRead = deleteOnClose;
        }

        private bool open()
        {
            this.workBook = new XLWorkbook(base.FileStream);
            this.workSheet = workBook.Worksheet(this.WorkSheetName);

            return (this.workSheet != null);
        }
        public bool openRead()
        {
            return (base.openFileRead() && this.open());
        }

        public bool openWrite()
        {
            return (base.openFileWrite() && this.open());
        }

        public bool close()
        {
            if (this.FileAccessMode == FileAccessMode.WRITE) this.workBook.Save();
            return base.closeFile();
        }

    }

    [DataContract]
    public class FormatSource
    {
        public FormatSource(SpreadSheet spreadSheet, int startAtRowIndex, int startAtColIndex)
        {
            this.SpreadSheet = spreadSheet;
            this.StartAtRowIndex = startAtRowIndex;
            this.StartAtColIndex = startAtColIndex;
        }

       
        public FormatSource(SpreadSheet spreadSheet, int startAtRowIndex, int startAtColIndex, KeyMap keyMap)
        {
            this.SpreadSheet = spreadSheet;
            this.StartAtRowIndex = startAtRowIndex;
            this.StartAtColIndex = startAtColIndex;
            this.KeyMap = keyMap;
        }

        [DataMember]
        public KeyMap KeyMap { get; set; }

        [DataMember]
        public SpreadSheet SpreadSheet { get; set; }

        [DataMember]
        public int StartAtRowIndex { get; set; }

        [DataMember]
        public int StartAtColIndex { get; set; }

        public File TargetFile { get; set; }
    }

    [DataContract]
    public class FlatFile : File
    {
        public FlatFile(string filePathNameExt) : base(filePathNameExt) { }

        public bool openRead()
        {
            return base.openFileRead();
        }

        public bool openWrite()
        {
            return base.openFileWrite();
        }

        public bool close()
        {
            return base.closeFile();
        }
    }

    [DataContract]
    public abstract class FlatFileDelimiters
    {
        protected FlatFileEscapeSequence[] escapeSequence;

        private char delimiterForRows = '\n';
        private char delimiterForCols = ',';

        [DataMember]
        public char DelimiterForRows
        {
            get { return delimiterForRows; }
            set { delimiterForRows = value; }
        }

        [DataMember]
        public char DelimiterForCols
        {
            get { return delimiterForCols; }
            set { delimiterForCols = value; }
        }

        [DataMember]
        public FlatFileEscapeSequence[] Replacements
        {
            get { return this.escapeSequence; }
            set { this.escapeSequence = value; }
        }



        public string applyTo(string inputBlock)
        {
            string resultBlock = inputBlock;

            foreach (var escapeSequence in this.escapeSequence)
            {
                resultBlock = string.IsNullOrEmpty(escapeSequence.IdentifySequenceBy) ? resultBlock :
                    resultBlock.Replace(escapeSequence.IdentifySequenceBy, escapeSequence.ReplaceSequenceWith);

                resultBlock = string.IsNullOrEmpty(escapeSequence.IdentifyDelimiterBy) ? resultBlock :
                    resultBlock.Replace(escapeSequence.IdentifyDelimiterBy, escapeSequence.ReplaceDelimiterWith);
            }

            return resultBlock;
        }
    }

    [DataContract]
    public class FlatFileExportDelimiters : FlatFileDelimiters
    {
        public FlatFileExportDelimiters()
        {
            base.escapeSequence = new FlatFileEscapeSequence[]
            {
                new FlatFileEscapeSequence( "", "", "", "" ),
                new FlatFileEscapeSequence( "", "", "", "" )

                // new FlatFileEscapeSequence( "\"", "\"\"", "", "" ),
                // new FlatFileEscapeSequence( ",", ",", ",", "\",\""),
            };
        }
    }

    [DataContract]
    public class FlatFileImportDelimiters : FlatFileDelimiters
    {
        public FlatFileImportDelimiters()
        {
            base.escapeSequence = new FlatFileEscapeSequence[]
            {
                new FlatFileEscapeSequence( "\"\"", "\"", "", "" ),
                new FlatFileEscapeSequence( ",", ",", "\",\"", "," )
            };
        }
    }

    [DataContract]
    public class File
    {

        private string filePathNameExt = "";

        [DataMember]
        public string FilePathNameExt
        {
            get { return this.filePathNameExt; }
            set // { this.filePathNameExt = value; }

            {
                if ( value == null ) { this.filePathNameExt = ""; }
                else if ( value.StartsWith("<![CDATA[") && value.EndsWith( "]]>" ) )
                {
                    var pattern = "(\\<\\!\\[CDATA\\[)?([^(\\]\\]\\>)]*)";
                    var groups = new System.Text.RegularExpressions.Regex(pattern).Match(value).Groups;

                    this.filePathNameExt = (groups.Count > 2) ? groups[2].Value.Replace("\\\\", "\\") : value;
                }
                else { this.filePathNameExt = value; }
            }

        }

        [DataMember]
        public UserAccount UserAccount { get; set; }

        public System.IO.FileStream FileStream { get; set; }

        private FileAccessMode fileAccessMode;

        public FileAccessMode FileAccessMode { get { return this.fileAccessMode; } }

        private bool deleteAfterRead = false;

        [DataMember]
        public bool DeleteAfterRead
        {
            get { return this.deleteAfterRead; }
            set { this.deleteAfterRead = value; }
        }

        public File(string filePathNameExt) : this(filePathNameExt, null) { }

        private File(string filePathNameExt, UserAccount account)
        {
            this.filePathNameExt = filePathNameExt;
            this.UserAccount = account;
            this.fileAccessMode = FileAccessMode.CLOSED;
        }

        protected bool closeFile()
        {
            try
            {
                switch (this.fileAccessMode)
                {
                    case FileAccessMode.READ:
                        this.FileStream.Dispose();
                        // this.FileStream.Unlock(0, long.MaxValue);]
                        this.fileAccessMode = FileAccessMode.CLOSED;
                        if (this.deleteAfterRead) System.IO.File.Delete(this.filePathNameExt);
                        return true;

                    case FileAccessMode.WRITE:
                        this.FileStream.Dispose();
                        // this.FlocileStream.Unlock(0, long.MaxValue);
                        this.fileAccessMode = FileAccessMode.CLOSED;
                        return true;

                    default: return true;
                }
            }
            catch (Exception) { return false; }
        }

        protected bool openFileWrite()
        {
            closeFile();

            try
            {
                this.FileStream = System.IO.File.Open(this.FilePathNameExt, System.IO.FileMode.OpenOrCreate);
                // this.FileStream.Lock(0, long.MaxValue);
            }
            catch (Exception) { return false; }

            this.fileAccessMode = FileAccessMode.WRITE;
            return true;
        }

        protected bool openFileRead()
        {
            if ( this.fileAccessMode == FileAccessMode.WRITE ) return true;
                else closeFile();

            try
            {
                this.FileStream = System.IO.File.OpenRead(this.FilePathNameExt);
                // this.FileStream.Lock(0, long.MaxValue);
            }
            catch (Exception) { return false; }


            this.fileAccessMode = FileAccessMode.READ;
            return true;
        }

        public File copyFileTo(File target, CreateFileOptions createFileOptions)
        {
            int attemptCount = 1;
            string attemptFile = "";

            bool overwriteExist = (createFileOptions.FileExistsAction == WhenExistingFile.REPLACE_EXISTING);
            bool renameExisting = (createFileOptions.FileExistsAction == WhenExistingFile.RENAME_EXISTING);
            bool renameOnCreate = (createFileOptions.FileExistsAction == WhenExistingFile.RENAME_NEW);
            bool useExisting = (createFileOptions.FileExistsAction == WhenExistingFile.USE_EXISTING);

            if ( useExisting && target.exists() ) return target;

            File result = File.copyFile(this, target, overwriteExist);

            if (result != null) { return target; }
            else if (renameExisting && File.exists(target))
            {
                while ((attemptFile != null) && (result != null))
                {
                    attemptFile = createFileOptions.nextFilename(target.FilePathNameExt, attemptCount++);
                    result = File.copyFile(this, new File(attemptFile), false);
                }
            }
            else if (renameOnCreate && File.exists(target))
            {

                while ((attemptFile != null) && (result == null))
                {
                    attemptFile = createFileOptions.nextFilename(target.FilePathNameExt, attemptCount++);
                    result = File.copyFile(this, new File(attemptFile), false);
                }
            }
            else { return null; }
            return result;
        }

        public bool exists()
        {
            return File.exists(this);
        }

        public static bool exists(File file)
        {
            return (System.IO.File.Exists(file.FilePathNameExt));
        }

        private static File copyFile(File source, File target, bool overwrite)
        {
            //
            try
            {
                System.IO.File.Copy(source.FilePathNameExt, target.FilePathNameExt, overwrite);
                return (new File(target.FilePathNameExt));
            }
            //
            catch (Exception) { return null; }
        }
    }

    [DataContract]
    [Flags]
    public enum FileAccessMode
    {
        [EnumMember]
        CLOSED = 0,
        [EnumMember]
        DENIED = 1,
        [EnumMember]
        READ = 2,
        [EnumMember]
        WRITE = 4
    }

    [DataContract]
    public enum WhenExistingFile { [EnumMember] REPLACE_EXISTING, [EnumMember] RENAME_NEW, [EnumMember] RENAME_EXISTING, [EnumMember] USE_EXISTING, [EnumMember] ABORT_SAVE = 0 };


    [DataContract]
    public class Credential { }

    [DataContract]
    public class CurrentUser : Credential
    {
        public bool elevate { get; set; }
    }

    [DataContract]
    public class UserAccount : Credential
    {
        [DataMember]
        public string Domain { get; set; }

        [DataMember]
        public string User { get; set; }

        [DataMember]
        public string Password { get; set; }
    }

    [DataContract]
    public class FlatFileEscapeSequence
    {
        private string searchFor;
        private string replaceWith;
        private string oldDelimiter;
        private string newDelimiter;

        public FlatFileEscapeSequence(string searchFor, string replaceWith, string oldDelimiter, string newDelimiter)
        {
            this.searchFor = searchFor;
            this.replaceWith = replaceWith;
            this.oldDelimiter = oldDelimiter;
            this.newDelimiter = newDelimiter;
        }

        [DataMember]
        public string IdentifySequenceBy
        {
            get { return this.searchFor; }
            set { this.searchFor = value; }
        }

        [DataMember]
        public string ReplaceSequenceWith
        {
            get { return this.replaceWith; }
            set { this.replaceWith = value; }
        }

        [DataMember]
        public string IdentifyDelimiterBy
        {
            get { return this.oldDelimiter; }
            set { this.oldDelimiter = value; }
        }

        [DataMember]
        public string ReplaceDelimiterWith
        {
            get { return this.newDelimiter; }
            set { this.newDelimiter = value; }
        }
    }

    [DataContract]
    public class CreateFileOptions
    {
        private static readonly string APPEND_WHEN_EXISTING_FILE = ")";
        private static readonly string PREPEND_WHEN_EXISTING_FILE = " (";
        private static readonly string PAD_PARTIAL_COUNT_WITH_STRING = "0000";

        public string nextFilename(string filePathNameExt, int startCountAt)
        {
            if (startCountAt.ToString().Length > PAD_PARTIAL_COUNT_WITH_STRING.Length) return null;

            int clipCountAt = startCountAt.ToString().Length;
            string countFormat = (PAD_PARTIAL_COUNT_WITH_STRING + startCountAt).Substring(clipCountAt);
            string fileExt = filePathNameExt.Split('.')[filePathNameExt.Split('.').Count() - 1];
            string filePathName = (filePathNameExt + "||").Replace(("." + fileExt + "||"), "");
            string dotExt = (filePathName == fileExt) ? "" : ("." + fileExt);

            return (filePathName + PREPEND_WHEN_EXISTING_FILE + countFormat + APPEND_WHEN_EXISTING_FILE + dotExt) ;
        }

        [DataMember]
        public WhenExistingFile FileExistsAction { get; set; }
    }

    [DataContract]
    public class DataMappingResult
    {
        [DataMember]
        public Record[] Data { get; set; }

        [DataMember]
        public string ErrorMessageToUser { get; set; }

        [DataMember]
        public string ErrorMessageToLogs { get; set; }
    }
}