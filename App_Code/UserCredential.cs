﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

/// <summary>
/// Summary description for UserCredential
/// </summary>
public class UserCredential
{

    string userName, domain, password;

	public UserCredential( string userName, string domain, string password )
	{
    }

    public IntPtr getToken()
    {
        IntPtr token = new IntPtr();

        if (!NativeMethods.LogonUser(
            this.userName,
            this.domain,
            this.password,
            NativeMethods.LogonType.NewCredentials,
            NativeMethods.LogonProvider.Default,
            out token))
        {
            return token;
        }
        else return new IntPtr(); // fail
    }

    public bool requestAccess( IntPtr token )
    {
        try
        {
            IntPtr tokenDuplicate;

            if (!NativeMethods.DuplicateToken(
                token,
                NativeMethods.SecurityImpersonationLevel.Impersonation,
                out tokenDuplicate))
            {
                return false;
            }

            try
            {
                using (WindowsImpersonationContext impersonationContext =
                    new WindowsIdentity(tokenDuplicate).Impersonate())
                {
                    // Do stuff with your share here.

                    impersonationContext.Undo();
                    return true;
                }
            }
            finally
            {
                if (tokenDuplicate != IntPtr.Zero)
                {
                    if (!NativeMethods.CloseHandle(tokenDuplicate))
                    {
                        // Uncomment if you need to know this case.
                        ////throw new Win32Exception();
                    }
                }
            }
        }
        finally
        {
            if (token != IntPtr.Zero)
            {
                if (!NativeMethods.CloseHandle(token))
                {
                    // Uncomment if you need to know this case.
                    ////throw new Win32Exception();
                }
            }
        }
	}
}
